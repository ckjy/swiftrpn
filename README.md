## Swift RPN calculator

This is a fairly versatile implementation of an RPN calculator. I am
trying to teach myself Swift, and thought this was a good exercise as
part of my learning journey.

### Usage

To build and run the program:

```sh
swift run
```

To build and run tests:

```sh
swift test
```

In both cases, you can append `-c release` to the command to compile in
release mode, which compiles with optimisations enabled.

### Intentional design goals

In order to support various potential use cases for the RPN calculator,
I strove to make it highly extensible and reusable. In particular:

+ `Operand` and `Stack` are protocols. You can use _any_ type that
  conforms to those protocols; you don't have to use `Double` and
  `[Double]` as the types (though those types do work out of the box).

+ The core of the calculator is the `Dispatcher` struct. `Dispatcher`
  holds no mutable state: instead, the caller passes in a mutable
  `Stack` object to its `dispatch` method. This means you can share
  a single `Dispatcher` instance among multiple threads, as long as
  you use distinct `Stack` objects for each thread.

+ The creator of a `Dispatcher` instance defines the commands available
  in that instance. `Dispatcher` itself has no built-in commands. In
  particular, the dispatch table used in the command-line program is
  located in `Sources/SwiftRpnDemo/main.swift`. That file serves as a
  good example of all the kinds of things you can put in the dispatch
  table.

+ Because multiple threads can share a single `Dispatcher` instance,
  the main purpose of having multiple `Dispatcher` instances is to
  have a different dispatch table for each. You could very feasibly
  make a multi-user calculator where each user has a different set of
  available commands depending on their preferred calculator mode
  (basic, scientific, financial, etc.), for example.

+ The `Dispatcher` does absolutely no I/O. Inputs are passed in via
  the `dispatch` method, and successful results are returned from that
  method. Error conditions are signalled via exceptions, which have
  useful messages you can access via normal string conversion.

### Incidental design artifacts

The current shape of Swift has some inherent limitations, and I've had
to make concessions to some of those:

+ Most math functions are not available unless you import the `Darwin`
  or `Glibc` module. This is very system-specific and gross so I opted
  not to use any of those functions. Interestingly, many of these
  functions would have been available as Clang intrinsics so they're
  not even inherently system-specific.

+ Foundation's `Decimal` struct does not conform to `FloatingPoint`, so
  if you want to use it as an `Operand`, some extensions are necessary.
  I put some extensions in `Sources/SwiftRpnDemo/DecimalExtensions.swift`
  to allow `Decimal` to be used with the default dispatch table with no
  hassles.

+ The `exit` and `help` commands work by throwing an exception during
  dispatch (the latter is to break a circular dependency which would
  have been gnarly to try to solve in Swift). One side effect of this
  is that tokens following `help` (and `exit`, obviously) on the same
  line are dropped.
