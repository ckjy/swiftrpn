import SwiftRpn
import XCTest

private struct MyError : Error, CustomStringConvertible {
    let description = "Testing 1 2 3"
}

public class SwiftRpnTests : XCTestCase {
    public static let allTests = [
        ("testUnary", testUnary),
        ("testBinary", testBinary),
        ("testTernary", testTernary),
        ("testSpecial", testSpecial),
        ("testInvalid", testInvalid)
    ]

    private let dispatcher: Dispatcher<Double, [Double]> = [
        ["+"]: .binary(+),
        ["-"]: .binary(-),
        ["neg"]: .unary(-),
        ["*"]: .binary(*),
        ["/"]: .binary(/),
        ["inv"]: .unary { 1 / $0 },
        ["fma"]: .ternary { $2.addingProduct($0, $1) },
        ["hypot"]: .binary { ($0 * $0 + $1 * $1).squareRoot() },
        ["million"]: .nullary(const: 1e6),
        ["trillion"]: .nullary(const: 1e12),
        ["sqrt"]: .unary(guard: { $0 >= 0.0 }, method: Double.squareRoot),
        ["dup"]: .special { $0.append($0.last!) },
        ["rotate"]: .special { $0.append($0.removeFirst()) },
        ["throw"]: .special(throw: MyError())
    ]

    private func dispatch(tokens: [String], stack: inout [Double]) throws {
        for token in tokens {
            try dispatcher.dispatch(to: token, stack: &stack)
        }
    }

    private func assertDispatch(tokens: String..., expected: Double) {
        var stack = [Double]()
        try! dispatch(tokens: tokens, stack: &stack)
        XCTAssertEqual(stack, [expected])
    }

    private func assertDispatchThrows(tokens: String..., message: String) {
        var stack = [Double]()
        XCTAssertThrowsError(try dispatch(tokens: tokens, stack: &stack)) {
            XCTAssertEqual(String(describing: $0), message)
        }
    }

    func testUnary() {
        assertDispatch(tokens: "1", "neg", expected: -1)
        assertDispatch(tokens: "8", "inv", expected: 0.125)
        assertDispatch(tokens: "0.25", "sqrt", expected: 0.5)
        assertDispatch(tokens: "trillion", "sqrt", "sqrt", expected: 1000)
    }

    func testBinary() {
        assertDispatch(tokens: "1", "2", "+", expected: 3)
        assertDispatch(tokens: "3", "5", "*", expected: 15)
        assertDispatch(tokens: "trillion", "sqrt", "million", "-", expected: 0)
        assertDispatch(tokens: "3", "4", "hypot", expected: 5)
    }

    func testTernary() {
        assertDispatch(tokens: "3", "4", "-5", "fma", expected: 7)
    }

    func testSpecial() {
        assertDispatch(tokens: "10", "dup", "*", expected: 100)
        assertDispatch(tokens: "10", "dup", "6", "-", "/", expected: 2.5)
        assertDispatch(tokens: "3", "4", "-5", "rotate", "fma", expected: -17)
        assertDispatchThrows(tokens: "throw", message: "Testing 1 2 3")
    }

    func testInvalid() {
        assertDispatchThrows(tokens: "0", "inv", message: "Operation inv returned invalid results [inf]")
        assertDispatchThrows(tokens: "-1", "sqrt", message: "Operation sqrt does not accept operands [-1.0]")
        assertDispatchThrows(tokens: "-1", "0", "/", message: "Operation / returned invalid results [-inf]")
        assertDispatchThrows(tokens: "billion", message: "I don't understand \"billion\"; enter ? for help")
        assertDispatchThrows(tokens: "3", "4", "fma", message: "Operator fma needs 3 arguments on the stack")
    }
}
