// swift-tools-version:4.2
import PackageDescription

let package = Package(
        name: "SwiftRpn",
        products: [
            .library(name: "SwiftRpn", targets: ["SwiftRpn"]),
            .executable(name: "SwiftRpnDemo", targets: ["SwiftRpnDemo"])
        ],
        targets: [
            .target(name: "SwiftRpn", dependencies: []),
            .target(name: "SwiftRpnDemo", dependencies: ["SwiftRpn"]),
            .testTarget(name: "SwiftRpnTests", dependencies: ["SwiftRpn"])
        ])
