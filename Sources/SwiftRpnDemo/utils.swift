import Foundation

private let whitespaces = CharacterSet.whitespaces

func isWhitespace(char: Character) -> Bool {
    return char.unicodeScalars.allSatisfy { whitespaces.contains($0) }
}

func readLine(prompt: String, strippingNewline: Bool = true) -> String? {
    print(prompt, terminator: " ")
    return readLine(strippingNewline: strippingNewline)
}
