import Foundation
import SwiftRpn

private let half = Decimal(0.5)
private let randDivisor = Decimal.greatestFiniteMagnitude.significand + 1
private let twoPow64 = Decimal(_exponent: 0, _length: 5, _isNegative: 0,
        _isCompact: 1, _reserved: 0, _mantissa: (0, 0, 0, 0, 1, 0, 0, 0))

extension Decimal : Operand {
    public init?(_ text: String) {
        self.init(string: text)
    }
}

extension Decimal {
    private func guessSquareRoot() -> Decimal {
        return Decimal(NSDecimalNumber(decimal: self).doubleValue.squareRoot())
    }

    func squareRoot() -> Decimal {
        let guess = guessSquareRoot()
        return guess.isFinite ? half * (guess + self/guess) : guess
    }
}

extension Decimal {
    mutating func formTruncatingRemainder(dividingBy other: Decimal) {
        guard !other.isZero else { self = .nan; return }
        var quotient = self / other
        var roundedQuotient = quotient
        // Round towards zero
        NSDecimalRound(&roundedQuotient, &quotient, 0, quotient >= 0 ? .down : .up)
        self -= other * roundedQuotient
    }

    func truncatingRemainder(dividingBy other: Decimal) -> Decimal {
        var lhs = self
        lhs.formTruncatingRemainder(dividingBy: other)
        return lhs
    }
}

extension Decimal {
    mutating func addProduct(_ lhs: Decimal, _ rhs: Decimal) {
        self += lhs * rhs
    }

    func addingProduct(_ lhs: Decimal, _ rhs: Decimal) -> Decimal {
        var addend = self
        addend.addProduct(lhs, rhs)
        return addend
    }
}

extension Decimal {
    static func random<T : RandomNumberGenerator>(in range: Range<Decimal>,
            using generator: inout T) -> Decimal {
        precondition(!range.isEmpty, "Can't get random value with an empty range")
        let delta = range.upperBound - range.lowerBound
        precondition(delta.isFinite, "There is no uniform distribution on an infinite range")

        let unitRandom = (twoPow64 * Decimal(generator.next()) + Decimal(generator.next())) / randDivisor
        return delta * unitRandom + range.lowerBound
    }

    static func random(in range: Range<Decimal>) -> Decimal {
        var g = SystemRandomNumberGenerator()
        return random(in: range, using: &g)
    }
}
