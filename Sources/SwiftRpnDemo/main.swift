import SwiftRpn

private enum SpecialRequests : Error {
    case exit, help
}

private let helpArityOrder = [
    ("Nullary", 0),
    ("Unary", 1),
    ("Binary", 2),
    ("Ternary", 3),
    ("Special", -1)
]

private typealias Type = Float80
private typealias Stack = [Type]

private var stderr = StdErr()

private let dispatcher: Dispatcher<Type, Stack> = [
    ["+"]: .binary(+),
    ["−", "-"]: .binary(-),
    ["×", "*"]: .binary(*),
    ["÷", "/"]: .binary(guard: { $1 != 0.0 }, /),
    ["%"]: .binary(guard: { $1 != 0.0 }, method: Type.truncatingRemainder),
    ["neg"]: .unary(-),
    ["√", "sqrt"]: .unary(guard: { $0 >= 0.0 }, method: Type.squareRoot),
    ["+×", "+*"]: .ternary(method: Type.addingProduct),
    ["×+", "*+", "fma"]: .ternary { $2.addingProduct($0, $1) },
    ["π", "pi"]: .nullary(const: .pi),

    ["rand0"]: .nullary { Type.random(in: 0.0..<1.0) },
    ["rand1"]: .unary(guard: { $0 > 0.0 }) { Type.random(in: 0.0..<$0) },
    ["rand2"]: .binary(guard: { $0 < $1 }) { Type.random(in: $0..<$1) },

    // The symbolic names for these operators are all based on GolfScript:
    // http://www.golfscript.com/golfscript/builtin.html
    [".", "dup"]: .unary { [$0, $0] },
    ["\\", "swap"]: .binary { [$1, $0] },
    [";", "pop"]: .unary { _ in [] },
    ["@", "rotate3"]: .ternary { [$1, $2, $0] },

    ["exit", "quit", "q"]: .special(throw: SpecialRequests.exit),
    ["?", "help"]: .special(throw: SpecialRequests.help),
    ["clear"]: .special { $0.removeAll() },
    ["dump"]: .special { print($0, to: &stderr) }
]

private var stack = Stack()

print("Enter RPN tokens separated by whitespace, or ? for available commands.")
while let line = readLine(prompt: ">") {
    do {
        var lastResult: Type? = nil
        for token in line.split(whereSeparator: isWhitespace) {
            lastResult = try dispatcher.dispatch(to: String(token), stack: &stack)
        }
        if let result = lastResult {
            print(result)
        }
    } catch SpecialRequests.exit {
        break
    } catch SpecialRequests.help {
        print("Available commands:")
        let cba = dispatcher.commandsByArity
        for (label, arity) in helpArityOrder {
            print("  \(label) operators:")
            for command in cba[arity] ?? [] {
                print("    \(command)")
            }
        }
    } catch {
        print("Error: \(error)", to: &stderr)
    }
}
print()
