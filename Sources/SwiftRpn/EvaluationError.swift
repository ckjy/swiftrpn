public enum EvaluationError : Error {
    case invalid_operator(String)
    case stack_underflow(operator: String, needed: Int)
    case invalid_operands(operator: String, operands: [Any])
    case invalid_result(operator: String, values: [Any])
}

extension EvaluationError : CustomStringConvertible {
    public var description: String {
        switch self {
        case let .invalid_operator(token):
            return "I don't understand \"\(token)\"; enter ? for help"
        case let .stack_underflow(operator: name, needed: n):
            return "Operator \(name) needs \(n) arguments on the stack"
        case let .invalid_operands(operator: name, operands: operands):
            return "Operation \(name) does not accept operands \(operands)"
        case let .invalid_result(operator: name, values: values):
            return "Operation \(name) returned invalid results \(values)"
        }
    }
}
