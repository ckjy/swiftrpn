public enum Operator<T : Operand, S : Stack> where S.Element == T {
    case nullary(Nullary<T, S>)
    case unary(Unary<T, S>)
    case binary(Binary<T, S>)
    case ternary(Ternary<T, S>)
    case special(Special<T, S>)
}

extension Operator {
    public var arity: Int {
        switch self {
        case .nullary: return 0
        case .unary: return 1
        case .binary: return 2
        case .ternary: return 3
        case .special: return -1
        }
    }
}
