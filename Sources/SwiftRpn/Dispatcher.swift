public struct Dispatcher<T : Operand, S : Stack> where S.Element == T {
    private let map: [String: Operator<T, S>]
    private let commands: [Command]
}

extension Dispatcher : ExpressibleByDictionaryLiteral {
    public init(dictionaryLiteral entries: ([String], Operator<T, S>)...) {
        map = Dictionary(uniqueKeysWithValues: entries.flatMap { (keys, value) in
            keys.map { ($0, value) }
        })

        commands = entries.map { (keys, _) in keys }
                .filter { !$0.isEmpty }
                .map { Command(name: $0.first!, aliases: Array($0.dropFirst())) }
                .sorted { lhs, rhs in lhs.name < rhs.name }
    }
}

extension Dispatcher {
    public var commandsByArity: [Int: [Command]] {
        return Dictionary(grouping: commands) { map[$0.name]!.arity }
    }
}

extension Dispatcher {
    private static func dispatch<OP : OperatorProtocol>(op: OP, to name: String,
            stack: inout S) throws -> T? where OP.Element == T, OP.Stack == S {
        let cookie = try op.precheck(name: name, stack: &stack)
        return try op.run(name: name, stack: &stack, cookie: cookie)
    }

    private static func dispatch(_ op: Operator<T, S>, to name: String,
            stack: inout S) throws -> T? {
        switch (op) {
        case let .nullary(nullary): return try dispatch(op: nullary, to: name, stack: &stack)
        case let .unary(unary): return try dispatch(op: unary, to: name, stack: &stack)
        case let .binary(binary): return try dispatch(op: binary, to: name, stack: &stack)
        case let .ternary(ternary): return try dispatch(op: ternary, to: name, stack: &stack)
        case let .special(special): return try dispatch(op: special, to: name, stack: &stack)
        }
    }

    @discardableResult
    public func dispatch(to name: String, stack: inout S) throws -> T? {
        if let num = T(name), num.isFinite {
            stack.append(num)
            return num
        } else if let op = map[name] {
            return try Dispatcher<T, S>.dispatch(op, to: name, stack: &stack)
        } else {
            throw EvaluationError.invalid_operator(name)
        }
    }
}
