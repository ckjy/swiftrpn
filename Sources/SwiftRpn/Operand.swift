public protocol Operand {
    init?(_ text: String)
    var isFinite: Bool { get }
}

extension Float : Operand {}
extension Double : Operand {}
extension Float80 : Operand {}
