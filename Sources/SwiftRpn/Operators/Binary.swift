public struct Binary<T : Operand, S : Stack> : StandardOperator where S.Element == T {
    typealias Element = T
    typealias Stack = S
    typealias ArgTuple = (first: T, second: T)

    let arity = 2

    let `guard`: ((T, T) -> Bool)?
    let function: (T, T) -> [T]

    func fetchArgs(stack: inout S) -> ArgTuple? { return stack.popLast2() }
    func isValid(args: ArgTuple) -> Bool { return `guard`.map { $0(args.first, args.second) } != false }
    func call(args: ArgTuple) -> [T] { return function(args.first, args.second) }
    func unpack(args: ArgTuple) -> [T] { return [args.first, args.second] }
}

extension Operator {
    public static func binary(guard: ((T, T) -> Bool)? = nil, _ function: @escaping (T, T) -> [T]) -> Operator {
        return binary(Binary(guard: `guard`, function: function))
    }

    public static func binary(guard: ((T, T) -> Bool)? = nil, _ function: @escaping (T, T) -> T) -> Operator {
        return binary(guard: `guard`) { x, y in [function(x, y)] }
    }

    public static func binary(guard: ((T, T) -> Bool)? = nil, method: @escaping (T) -> (T) -> [T]) -> Operator {
        return binary(guard: `guard`) { x, y in method(x)(y) }
    }

    public static func binary(guard: ((T, T) -> Bool)? = nil, method: @escaping (T) -> (T) -> T) -> Operator {
        return binary(guard: `guard`) { x, y in [method(x)(y)] }
    }
}
