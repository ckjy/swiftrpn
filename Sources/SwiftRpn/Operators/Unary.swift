public struct Unary<T : Operand, S : Stack> : StandardOperator where S.Element == T {
    typealias Element = T
    typealias Stack = S

    let arity = 1

    let `guard`: ((T) -> Bool)?
    let function: (T) -> [T]

    func fetchArgs(stack: inout S) -> T? { return stack.popLast() }
    func isValid(args arg: T) -> Bool { return `guard`.map { $0(arg) } != false }
    func call(args arg: T) -> [T] { return function(arg) }
    func unpack(args arg: T) -> [T] { return [arg] }
}

extension Operator {
    public static func unary(guard: ((T) -> Bool)? = nil, _ function: @escaping (T) -> [T]) -> Operator {
        return unary(Unary(guard: `guard`, function: function))
    }

    public static func unary(guard: ((T) -> Bool)? = nil, _ function: @escaping (T) -> T) -> Operator {
        return unary(guard: `guard`) { x in [function(x)] }
    }

    public static func unary(guard: ((T) -> Bool)? = nil, method: @escaping (T) -> () -> [T]) -> Operator {
        return unary(guard: `guard`) { x in method(x)() }
    }

    public static func unary(guard: ((T) -> Bool)? = nil, method: @escaping (T) -> () -> T) -> Operator {
        return unary(guard: `guard`) { x in [method(x)()] }
    }
}
