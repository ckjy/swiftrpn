protocol StandardOperator : OperatorProtocol {
    associatedtype ArgTuple

    var arity: Int { get }

    func fetchArgs(stack: inout Stack) -> ArgTuple?
    func isValid(args: ArgTuple) -> Bool
    func call(args: ArgTuple) -> [Element]
    func unpack(args: ArgTuple) -> [Element]
}

extension StandardOperator {
    func precheck(name: String, stack: inout Stack) throws -> ArgTuple {
        guard let args = fetchArgs(stack: &stack) else {
            throw EvaluationError.stack_underflow(operator: name, needed: arity)
        }
        guard isValid(args: args) else {
            throw EvaluationError.invalid_operands(operator: name, operands: unpack(args: args))
        }
        return args
    }

    func run(name: String, stack: inout Stack, cookie args: ArgTuple) throws -> Element? {
        let results = call(args: args)
        guard results.allSatisfy({ $0.isFinite }) else {
            throw EvaluationError.invalid_result(operator: name, values: results)
        }
        stack.append(contentsOf: results)
        return results.last
    }
}
