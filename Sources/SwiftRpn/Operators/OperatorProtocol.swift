protocol OperatorProtocol {
    associatedtype Element : Operand
    associatedtype Stack : SwiftRpn.Stack where Stack.Element == Element
    associatedtype Cookie

    func precheck(name: String, stack: inout Stack) throws -> Cookie
    func run(name: String, stack: inout Stack, cookie: Cookie) throws -> Element?
}
