public struct Ternary<T : Operand, S : Stack> : StandardOperator where S.Element == T {
    typealias Element = T
    typealias Stack = S
    typealias ArgTuple = (first: T, second: T, third: T)

    let arity = 3

    let `guard`: ((T, T, T) -> Bool)?
    let function: (T, T, T) -> [T]

    func fetchArgs(stack: inout S) -> ArgTuple? { return stack.popLast3() }
    func isValid(args: ArgTuple) -> Bool { return `guard`.map { $0(args.first, args.second, args.third) } != false }
    func call(args: ArgTuple) -> [T] { return function(args.first, args.second, args.third) }
    func unpack(args: ArgTuple) -> [T] { return [args.first, args.second, args.third] }
}

extension Operator {
    public static func ternary(guard: ((T, T, T) -> Bool)? = nil, _ function: @escaping (T, T, T) -> [T]) -> Operator {
        return ternary(Ternary(guard: `guard`, function: function))
    }

    public static func ternary(guard: ((T, T, T) -> Bool)? = nil, _ function: @escaping (T, T, T) -> T) -> Operator {
        return ternary(guard: `guard`) { x, y, z in [function(x, y, z)] }
    }

    public static func ternary(guard: ((T, T, T) -> Bool)? = nil, method: @escaping (T) -> (T, T) -> [T]) -> Operator {
        return ternary(guard: `guard`) { x, y, z in method(x)(y, z) }
    }

    public static func ternary(guard: ((T, T, T) -> Bool)? = nil, method: @escaping (T) -> (T, T) -> T) -> Operator {
        return ternary(guard: `guard`) { x, y, z in [method(x)(y, z)] }
    }
}
