public struct Special<T : Operand, S : Stack> : OperatorProtocol where S.Element == T {
    typealias Element = T
    typealias Stack = S
    typealias Cookie = Void

    let `guard`: ((String, S) throws -> Void)?
    let function: ((inout S) -> Void)?

    func precheck(name: String, stack: inout S) throws {
        try `guard`.map { try $0(name, stack) }
    }

    func run(name: String, stack: inout S, cookie: Void) -> T? {
        function.map { $0(&stack) }
        return nil
    }
}

extension Operator {
    public static func special(guard: ((String, S) throws -> Void)? = nil,
            function: ((inout S) -> Void)? = nil) -> Operator {
        return special(Special(guard: `guard`, function: function))
    }

    public static func special<E : Error>(throw error: E) -> Operator {
        return special(Special(guard: { _, _ in throw error }, function: nil))
    }
}
