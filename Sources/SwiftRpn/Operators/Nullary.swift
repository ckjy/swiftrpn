public struct Nullary<T : Operand, S : Stack> : StandardOperator where S.Element == T {
    typealias Element = T
    typealias Stack = S

    let arity = 0

    let `guard`: (() -> Bool)?
    let function: () -> [T]

    func fetchArgs(stack: inout S) -> ()? { return () }
    func isValid(args: ()) -> Bool { return `guard`.map { $0() } != false }
    func call(args: ()) -> [T] { return function() }
    func unpack(args: ()) -> [T] { return [] }
}

extension Operator {
    public static func nullary(guard: (() -> Bool)? = nil, _ function: @escaping () -> [T]) -> Operator {
        return nullary(Nullary(guard: `guard`, function: function))
    }

    public static func nullary(guard: (() -> Bool)? = nil, _ function: @escaping () -> T) -> Operator {
        return nullary(guard: `guard`) { [function()] }
    }

    public static func nullary(guard: (() -> Bool)? = nil, const value: T) -> Operator {
        return nullary(guard: `guard`) { [value] }
    }
}
