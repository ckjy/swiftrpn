public struct Command {
    let name: String
    let aliases: [String]
}

extension Command : TextOutputStreamable {
    public func write<Target : TextOutputStream>(to target: inout Target) {
        target.write(name)
        if !aliases.isEmpty {
            target.write(" (")
            var first = true
            for alias in aliases {
                if (first) {
                    first = false
                } else {
                    target.write(", ")
                }
                target.write(alias)
            }
            target.write(")")
        }
    }
}
