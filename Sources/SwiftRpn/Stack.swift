public protocol Stack {
    associatedtype Element

    mutating func append(_: Element)
    mutating func append<S : Sequence>(contentsOf: S) where S.Element == Element

    mutating func popLast() -> Element?
    mutating func popLast2() -> (Element, Element)?
    mutating func popLast3() -> (Element, Element, Element)?
}

extension Stack where Self : RandomAccessCollection & RangeReplaceableCollection {
    private mutating func popLastN(_ n: Int) -> [Element]? {
        guard let start = index(endIndex, offsetBy: -n, limitedBy: startIndex) else { return nil }
        let range = start..<endIndex
        let result = Array(self[range])
        removeSubrange(range)
        return result
    }

    public mutating func popLast2() -> (Element, Element)? {
        guard let result = popLastN(2) else { return nil }
        return (result[0], result[1])
    }

    public mutating func popLast3() -> (Element, Element, Element)? {
        guard let result = popLastN(3) else { return nil }
        return (result[0], result[1], result[2])
    }
}
